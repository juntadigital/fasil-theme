/*
 * Application
 */

/*
 * Implement nice invalid form fields
 */
$(window).on('ajaxInvalidField', function(event, fieldElement, fieldName, errorMsg, isFirst) {
    var $field = $(fieldElement).closest('.form-group') || $(fieldElement).closest('.form-check')
    var $input_text = $($field).find('.form-control')
    var $checkbox = $($field).find('.checkbox-validation')
        // $help = $('<div />').addClass('invalid-feedback')

    
    if($input_text.length > 0)
    {
        var $input = $input_text
    }else{
        var $input = $checkbox
    }    

    $help = $('<div />').addClass('invalid-feedback')
    if (!$field.length) {
        return
    }

    event.preventDefault()

    if (errorMsg) {
        console.log(errorMsg);
        $help.text(errorMsg.join(', '))
    }

    $help.addClass('form-field-error-label')
    $input.addClass('is-invalid').removeClass('is-valid')

    if(fieldName == 'email')
    {
        $('#sucesso').remove()
    }

    // Prevent the next error alert only once
    $(window).one('ajaxErrorMessage', function(event, message){
        event.preventDefault()
    })

    if ($('.form-field-error-label', $field).length > 0)
        return
    $field.append($help)

    // Scroll to the form group
    if (isFirst) {
        $('html, body').animate({ scrollTop: $field.offset().top }, 500, function(){
            fieldElement.focus()
        })
    }
})

$(document).on('ajaxPromise', '[data-request]', function() {
    var $form = $(this).closest('form'),
        $help = $('.form-field-error-label', $form)

    if (!$form.length || !$help.length)
        return

    $help.remove()
    $($form).find('.is-invalid').removeClass('is-invalid')
})